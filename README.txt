trans~ v0.1a

This library implements transaural system in Pure Data.
It works with Pd-Extended and Pd vanilla.
This library recieves 4 parameters (binaural sound for right ear, binaural sound for left ear, loudspeaker's azimuth and distance) and outputs sound for left loudspeaker and sound for right  loudspeaker.
if you don't have a library to create binaural sound, you can use hrir~ library (https://bitbucket.org/julovi/hrir).

COMPILE
Mac : Use make command.
 
Windows : Now, we have not create make fime for windows. Please use developer command prompt and move this project directory. After that, use this command (cl /LD trans~.c sqlite3.lib pd.lib libfftw3-3.lib /link /export:trans_tilde_setup).


DEMO
We uploaded a demo movie on Youtube (https://www.youtube.com/playlist?list=PLtymDorbtiCnOeZUArNZUa2Rgeq-Xdsvn). 


This library provides transaural spatialization via head-related impulse responses
originally recorded by Prof. Qu and colleagues as reported at IEEE TRANSACTIONS ON AUDIO, SPEECH, AND LANGUAGE PROCESSING, VOL. 17, NO. 6, AUGUST 2009. The HRIR used here however, are diffuse field equalized, and minimum-phase filtered. Also, only the right hemisphere is used.

This library depends on sqlite3 and FFTW3 libraries.
