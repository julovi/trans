/** \mainpage Transaural object for Pure-data: trans~
	\section Description
	TThis object receives a binaural signal in its two audio inlets, and the azimuth and distance of a symmetric stereo loudspeaker array (elevation is considered zero degrees) and implements a simple transaural object by using a shuffler filter.

	\section Licence

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Created by Takaya Ninagawa with Julian Villegas on 10/1/2016.

	@author Julian Villegas <julian ^_^ at ^_^ u-aizu (*) ac (*) jp>
	@version 0.1a
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <unistd.h>
//To compile it for Pd on Mac
# define PDMAC_VERSION

#ifdef WIN_VERSION
#include <io.h>
#endif

#include "m_pd.h"
#include "sqlite3.h"
#include <fftw3.h>

#define DEFAULT_N_POINTS 256
#define MAX_N_POINTS 3000
#define MAX_BLOCKSIZE 8192
#define MAX_SAMPLES 4096
#define F_OK    0
#define DBFILENAME "/HRIR@44100.db"
#define MyPI 3.14159265358979323846


static t_class *trans_class;

typedef struct _trans {

    t_object x_obj;

    t_sample f;

    t_float sr; ///The sampling rate

    t_sample *Xl;
    t_sample *Xr;

    fftw_complex Fa[MAX_SAMPLES];
    fftw_complex Fb[MAX_SAMPLES];

    t_inlet *in_x2;

    //outlet
    t_outlet *h_outputL;    // output left  speaker to ear left
    t_outlet *h_outputR;    // output left  speaker to ear right

    t_int nPts;     /// No. of taps used for the convolution
    t_int nPtsInDb; /// No. of taps existing in the database (sampling rate dependent)
    t_float azimuth;   ///inlet from 0 to 359 degrees
    t_float elevation; ///inlet from -40 to 90 degree
    t_float distance;     ///inlet from 20 to 90 degree

    t_float z; //signal?
    t_float g; //broadband attenuation
    t_int   m; //ITD in samples

    t_float azi; ///azimuth value
    t_float ele; ///elevation value
    t_float dis; ///distance value
    t_float rs[2]; /// nearest measured distance for a given point
    t_float es[2]; /// nearest measured elevation for a given point
    t_float as[2]; /// nearest measured azimuth for a given point
    t_float on[3]; /// flag to indicate when a point concide with a measurement
    t_float set[9][3]; /// measurements candidate for interpolation in  this order: rng, ele, azi
    t_float dist[9]; /// Distances from the measurements to the point
    t_int n_meas; /// number of candidates for interpolation

    t_float prevAzi; ///old azimuth value
    t_float prevEle; ///old elevation value
    t_float prevRng; ///old distance value
    double *window;
    float hrtf[MAX_N_POINTS];

    t_float OUT[2][MAX_N_POINTS]; //0 = LEFT, 1 = RIGHT

    char *zErrMsg;
    char *zHrtfErrMsg;

    t_float previousImpulse[2][MAX_N_POINTS]; // one more for the interpolation
    t_float currentImpulse[9][2][MAX_N_POINTS]; // one more for the interpolation
    int currentRow; // For the IR extraction
    t_float convBufferC[MAX_N_POINTS];
    t_float convBufferI[MAX_N_POINTS];

    t_float low_filter_hz;

    char path[2000];
    t_float crossCoef[MAX_BLOCKSIZE];
    t_int bufferPin;

    //fftw
    int fftsize;
    fftw_plan fftplanL;
    double *fftinL;
    fftw_complex *fftoutL;
    fftw_plan fftplanR;
    double *fftinR;
    fftw_complex *fftoutR;

    //invfftw
    fftw_plan fftplanInvL;
    fftw_complex *fftinInvL;
    double *fftoutInvL;
    fftw_plan fftplanInvR;
    fftw_complex *fftinInvR;
    double *fftoutInvR;

    //hrirfft
    fftw_plan fftplanHrirL;
    double *fftinHrirL;
    fftw_complex *fftoutHrirL;
    fftw_plan fftplanHrirR;
    double *fftinHrirR;
    fftw_complex *fftoutHrirR;

    //DB
    sqlite3 *db;
    t_int connected;
    t_int rc;

    sqlite3 *hrtfDb;
    t_int hrtfConnected;
    t_int hrtfRc;

} t_trans;


// adapted from jsarlo's windowing library
// Hanning
static void makewindow(double *w, int n) {
    int i;
    double xshift = n / 2.0;
    double x;
    for (i = 0; i < n; i++) {
        x = (i - xshift) / xshift;
        w[i] = 0.5 * (1 + cos(MyPI * x));
    }

}
#include "transFunctions.h"


/**
* Process the convolution of the incoming audio
*/
t_int *trans_perform(t_int *w) {


    t_trans *x = (t_trans *)(w[1]);
    x->Xl = (t_sample *)(w[2]);
    x->Xr = (t_sample *)(w[3]);
    t_sample  *outR = (t_sample *)(w[5]);
    t_sample  *outL = (t_sample *)(w[4]);
    float tmpR;
    float tmpL;
    int blocksize = (int)(w[6]);

    if (x->connected == 1) {
        x->prevAzi = x->azi; /// Save last values to skip unnecessary computations
        x->prevEle = x->ele;
        x->prevRng = x->dis;

        x->azi = roundf(x->azimuth);
        x->ele = roundf(x->elevation);
        x->dis = roundf(x->distance);
        if ((x->prevAzi == x->azi) && (x->prevEle == x->ele) && (x->prevRng == x->dis)) {
            //Do not find the filter
        } else {
            rangeDis(x);
            rangeEle(x);
            rangeAzi(x);
            formSet(x);
            testCoplan_lin(x);
        }

        #pragma region

        float tmp;
        for (int i = 0; i < blocksize + 512 -1; i++) {
            if (i < blocksize) {
                tmp = x->Xl[i];
                x->fftinL[i] = ((x->Xr[i] + x->Xl[i]))/2;
                x->fftinR[i] = ((tmp - x->Xr[i]) )/2;
            } else {
                x->fftinL[i] = 0;
                x->fftinR[i] = 0;
            }

            if (i < 512) {
                //x->fftinHrirL[i] = x->currentImpulse[x->currentRow][0][i];
                //x->fftinHrirR[i] = x->currentImpulse[x->currentRow][1][i];
                x->fftinHrirL[i] = 1 / (x->currentImpulse[x->currentRow][0][i] + x->currentImpulse[x->currentRow][1][i]);
                x->fftinHrirR[i] = 1 / (x->currentImpulse[x->currentRow][0][i] - x->currentImpulse[x->currentRow][1][i]);
                if (x->fftinHrirL[i] <= 0.000001 && x->fftinHrirR[i] <= 0.000001) {
                    //x->fftinHrirL[i] = 0.00001;
                    //x->fftinHrirR[i] = 0.00001;
                }
            } else {
                x->fftinHrirL[i] = 0;
                x->fftinHrirL[i] = 0;
            }
        }



        //FFT
        fftw_execute(x->fftplanL);
        fftw_execute(x->fftplanR);
        fftw_execute(x->fftplanHrirL);
        fftw_execute(x->fftplanHrirR);

        //multiple
        fftw_complex a[DEFAULT_N_POINTS];
        fftw_complex b[DEFAULT_N_POINTS];

        for (int i = 0; i < (blocksize + 512 - 1)/2+1; i++) {

            x->fftinInvL[i][0] = x->fftoutL[i][0] * x->fftoutHrirL[i][0] - x->fftoutL[i][1] * x->fftoutHrirL[i][1];
            x->fftinInvL[i][1] = x->fftoutL[i][0] * x->fftoutHrirL[i][1] + x->fftoutL[i][1] * x->fftoutHrirL[i][0];

            x->fftinInvR[i][0] = x->fftoutR[i][0] * x->fftoutHrirR[i][0] - x->fftoutR[i][1] * x->fftoutHrirR[i][1];
            x->fftinInvR[i][1] = x->fftoutR[i][0] * x->fftoutHrirR[i][1] + x->fftoutR[i][1] * x->fftoutHrirR[i][0];
        }


        // inv FFT
        fftw_execute(x->fftplanInvL);
        fftw_execute(x->fftplanInvR);

        // Outputs
        for (int i = 0; i < blocksize; i++) {
            tmp = x->fftoutInvL[i];
            x->fftoutInvL[i] += x->fftoutInvR[i];
            x->fftoutInvR[i] = tmp - x->fftoutInvR[i];
            *outL++ = x->fftoutInvL[i] / (x->fftsize*100000);
            *outR++ = x->fftoutInvR[i] / (x->fftsize * 100000);
        }

        #pragma endregion main program

    }
    return (w + 7);
}

/** Inform Pd that this object does DSP
* trans_perform: the callback function
* 6: number of parameters (listed ahead)
* x: userdata
* sp[0]->s_vec : in_samplesL
* sp[1]->s_vec : in_samplesR
* sp[2]->s_vec : audio_outR
* sp[3]->s_vec : audio_outL
* sp[0]->s_n : blocksize
*/
void trans_dsp(t_trans *x, t_signal **sp) {

    dsp_add(trans_perform, 6, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec,sp[3]->s_vec, sp[0]->s_n);

    #pragma region
    x->fftsize = (sp[0]->s_n+ 512 -1); //to have a blocksize fft output

    //set L
    x->fftinL = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftoutL = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex))/2+1);
    x->fftplanL = fftw_plan_dft_r2c_1d((x->fftsize), x->fftinL, x->fftoutL, FFTW_MEASURE);
    //set R
    x->fftinR = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftoutR = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex)) / 2 + 1);
    x->fftplanR = fftw_plan_dft_r2c_1d((x->fftsize), x->fftinR, x->fftoutR, FFTW_MEASURE);
    //set invL
    x->fftinInvL = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex)) / 2 + 1);
    x->fftoutInvL = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftplanInvL = fftw_plan_dft_c2r_1d((x->fftsize), x->fftinInvL, x->fftoutInvL, FFTW_MEASURE);
    //set invR
    x->fftinInvR = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex)) / 2 + 1);
    x->fftoutInvR = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftplanInvR = fftw_plan_dft_c2r_1d((x->fftsize), x->fftinInvR, x->fftoutInvR, FFTW_MEASURE);
    //set Hrir
    x->fftinHrirL = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftoutHrirL = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex)) / 2 + 1);
    x->fftinHrirR = fftw_alloc_real(x->fftsize*sizeof(double));
    x->fftoutHrirR = fftw_alloc_complex((x->fftsize*sizeof(fftw_complex)) / 2 + 1);
    x->fftplanHrirL = fftw_plan_dft_r2c_1d((x->fftsize), x->fftinHrirL, x->fftoutHrirL, FFTW_MEASURE);
    x->fftplanHrirR = fftw_plan_dft_r2c_1d((x->fftsize), x->fftinHrirR, x->fftoutHrirR, FFTW_MEASURE);

    #pragma endregion fftの初期化

    #pragma region
    x->window = getbytes(sizeof(double)* x->fftsize);
    makewindow(x->window, sp[0]->s_n);
    int power, base;
    x->sr = sp[0]->s_sr;
    char file[2000] = "";
    char str[8] = "";
#ifdef WIN_VERSION
    strcpy_s(file, _countof(file), x->path);
    strcat_s(file, _countof(file), "/HRIR@");
    sprintf_s(str, sizeof(str), "%d", (int)x->sr);
    strcat_s(file, _countof(file), str);
    strcat_s(file, _countof(file), ".db");
    if (_access(file, F_OK) == -1) { // file doesn't exist
#endif
#ifdef PDMAC_VERSION
        strcpy(file,  x->path);
        strcat(file,  "/HRIR@");
        sprintf(str,  "%d", (int)x->sr);
        strcat(file,  str);
        strcat(file,  ".db");
        if (access(file, F_OK) == -1) { // file doesn't exist
#endif

            post("Warning: The database %s doesn't exist,\ntrying to use %s database", file, DBFILENAME);
#ifdef WIN_VERSION
            strcpy_s(file, _countof(file), x->path);
            strcat_s(file, _countof(file), "/HRIR@");
            sprintf_s(str, sizeof(file), "%d", 44100);
            strcat_s(file, _countof(file), str);
            strcat_s(file, _countof(file), ".db");
            if (_access(file, F_OK) == -1) {
#endif
#ifdef PDMAC_VERSION
                strcpy(file, x->path);
                strcat(file,  "/HRIR@");
                sprintf(str,  "%d", 44100);
                strcat(file,  str);
                strcat(file,  ".db");
                if (access(file, F_OK) == -1) {
#endif
                    post("Error: The database %s doesn't exist either, trying downloading from \n http://arts.u-aizu.ac.jp/research/hrir-with-distance-control/");
                } else {
                    post("Warning: Using %s at a sampling freq. of %d, results are not correct.", file, (int)x->sr);
                }
            } else {
                if (x->connected == 1) {
                    sqlite3_close(x->db);
                    x->connected = 0;
                }
                x->rc = sqlite3_open(file, &(x->db));
                if (x->rc) {
                    post("Can't open database: %s : %s\n", file, sqlite3_errmsg(x->db));
                    sqlite3_close(x->db);
                } else {
                    post("%s database opened", file);
                    x->connected = 1;
                }

                switch ((int)(x->sr)) {
                case 8000:
                    // number of taps at 8000 = 125
                    x->nPtsInDb = 250;
                    break;
                case 16000:
                    // number of taps at 16000 = 250
                    x->nPtsInDb = 500;
                    break;
                case 22050:
                    //number of taps at 22050 = 344
                    x->nPtsInDb = 688;
                    break;
                case 44100:
                    // number of taps at 44100 = 689
                    x->nPtsInDb = 1378;
                    break;
                case 48000:
                    // number of taps at 48000 = 750
                    x->nPtsInDb = 1500;
                    break;
                case 65536:
                    // number of taps at 65536 = 1024
                    x->nPtsInDb = 2048;
                    break;
                case 96000:
                    // number of taps at 96000 = 1500
                    x->nPtsInDb = 3000;
                    break;
                case 192000:
                    // number of taps at 192000 = 3000
                    x->nPtsInDb = 6000;
                    break;
                default:
                    post("WARNING: Define the right amount of hrir taps for the  sampling rate: %0.0f Hz.\nConsider downloading a suitable database for this sampling rate.\n", x->sr);
                    break;
                }

                base = x->nPtsInDb / 2;
                power = 0;

                while (base > 1) {
                    base = base >> 1;
                    power += 1;
                }
                base = 1;
                while (power-- > 0) {
                    base = base << 1;
                }

                if (x->nPts > x->nPtsInDb / 2) {
                    x->nPts = base;
                }

                if (!isPowerOfTwo(x->nPts)) {
                    x->nPts = base;
                    post("Number of tabs must be a power of 2\n", base, x->nPts);

                }
                post("hrir~: Max. blocksize: 8192, Max. taps %d, currently using %d \n", base, x->nPts);
            }
            #pragma endregion DBの接続
        }

		/**
		* close the database when disposing the trans~ object and free fftw
		*/
        void trans_free(t_trans *x) {
            freebytes(x->window, sizeof(double)* x->fftsize);
            outlet_free(x->h_outputL);
            outlet_free(x->h_outputR);
            inlet_free(x->in_x2);

            if (x->connected) {
                sqlite3_close(x->db);
                post("measurement database closed");
                x->connected = 0;
            }

            #pragma region
            fftw_free(x->fftoutL);
            fftw_free(x->fftinL);
            fftw_destroy_plan(x->fftplanL);
            fftw_free(x->fftoutR);
            fftw_free(x->fftinR);
            fftw_destroy_plan(x->fftplanR);
            fftw_free(x->fftoutInvL);
            fftw_free(x->fftinInvL);
            fftw_destroy_plan(x->fftplanInvL);
            fftw_free(x->fftoutInvR);
            fftw_free(x->fftinInvR);
            fftw_destroy_plan(x->fftplanInvR);
            fftw_free(x->fftoutHrirL);
            fftw_free(x->fftinHrirL);
            fftw_destroy_plan(x->fftplanHrirL);
            fftw_free(x->fftoutHrirR);
            fftw_free(x->fftinHrirR);
            fftw_destroy_plan(x->fftplanHrirR);

            #pragma endregion free fft

        }

		/** Instantiate the object
		*
		* @param azimArg the azimuth
		* @param distanceArg the distance
		*/
        void *trans_new(void) {
            t_trans *x = (t_trans *)pd_new(trans_class);

            x->in_x2 = inlet_new(&x->x_obj,&x->x_obj.ob_pd,&s_signal,&s_signal);

            x->h_outputL = outlet_new(&x->x_obj, gensym("signal"));
            x->h_outputR = outlet_new(&x->x_obj, gensym("signal"));

            floatinlet_new(&x->x_obj, &x->azimuth);     /// azimuth
            floatinlet_new(&x->x_obj, &x->distance);   /// distance

            #pragma region init
            x->azi = 45;
            x->ele = 0; // so the first time the object is opened we have output
            x->dis = 140;
            x->nPtsInDb = 300;
            x->elevation = 0;
            x->azimuth = 0;
            x->distance = 20;

            x->nPts = 512;

            for (int i = 0; i < MAX_N_POINTS; i++) {
                x->convBufferC[i] = 0;
                x->convBufferI[i] = 0;
                x->previousImpulse[0][i] = 0;
                x->previousImpulse[1][i] = 0;

                x->OUT[0][i] = 0;
                x->OUT[1][i] = 0;


                x->crossCoef[i] = 1.0 * i / (MAX_BLOCKSIZE - 1.0);
                x->crossCoef[i] = cos((MyPI / 2) * ((float)i / (MAX_BLOCKSIZE - 1.0))); ///cosine-sine cross-fading
            }
#ifdef WIN_VERSION
            strcat_s(x->path, _countof(x->path), canvas_getdir(canvas_getcurrent())->s_name); /// The files should be in the same directory
#endif
#ifdef PDMAC_VERSION
            strcat(x->path, canvas_getdir(canvas_getcurrent())->s_name); /// The files should be in the same directory
#endif

            x->connected = 0;

            #pragma endregion 初期化

            return (x);

        }

		/**
		* setup the object
		*/
        void trans_tilde_setup(void) {

            trans_class = class_new(gensym("trans~"),
                                    (t_newmethod)trans_new,
                                    (t_method)trans_free, sizeof(t_trans),
                                    CLASS_DEFAULT, //a normal object with one inlet
                                    A_DEFSYMBOL, A_DEFSYMBOL, A_DEFSYMBOL, 0);//a numerical value

            //void class_addmethod(t_class *c, t_method fn, t_symbol *sel,t_atomtype arg1, ...);
            class_addmethod(trans_class,
                            (t_method)trans_dsp, gensym("dsp"), 0);

            //CLASS_MAINSIGNALIN(<class_name>, <class_data>, <f>);
            //The macro CLASS_MAINSIGNALIN declares, that the class will use signal-inlets.
            //The first macro - argument is a pointer to the signal - class.The second argument is the type of the class - data space.The third argument is a(dummy -)foating point - variable of the data space,
            CLASS_MAINSIGNALIN(trans_class, t_trans, f);
        }
